from django.apps import AppConfig


class HelloPostgresConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'hello_postgres'
