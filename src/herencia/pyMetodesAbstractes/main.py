from classes.Quadrat import Quadrat


def main():
    q = Quadrat(10, 20)
    print(q.calcula_area())

    print(q)


if __name__ == '__main__':
    main()
