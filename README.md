# Repositori del mòdul M15-DUAL
> Curs 2021 - 2022
> 
>  **[Institut Eugeni d'Ors](https://www.ies-eugeni.cat) - [Vilafranca del Penedès](https://goo.gl/maps/CX6KV3ZGNsR2)**

***Professorat***
* [Gabriela Apricopoae](https://www.linkedin.com/in/gabriela-apricopoae-a7448134/)
* [David Amorós](https://www.linkedin.com/in/david-amor%C3%B3s-56011360/)
* [Rubén Nadal](https://www.linkedin.com/in/rnadalb/)



## Recursos en línia

* Moodle del mòdul: [DAM2-M15-MD](http://www.ies-eugeni.cat/course/view.php?id=817)

## Presentació del mòdul
[Presentació M15 at Genially](https://view.genial.ly/613f667ca5a8670dfd629b58/presentation-presentacio-m15-dual)

