from django.shortcuts import render, get_object_or_404

# Create your views here.
from mvt_app.models import Config


def welcome(request):
    # objects es connectarà a la BBDD i ens retornarà informació
    # Consultar: https://docs.djangoproject.com/en/4.0/ref/models/class/
    n_configs = Config.objects.count()  # Nombre d'elements de la taula
    configs = Config.objects.all()
    return render(request, 'welcome.html', context={'n_configs': n_configs, 'configs': configs})


def config_detail(request, id_config):
    """
    Obtenim el registre de la taula tbl_config identificat amb la seva clau primària id_config
    :param request: petició web
    :param id_config: identificador (clau primària) de la taula de la que obtenim el registre
    :return: pàgina web amb el detall del registre obtingut
    """
    # Si es passa un registre no existent falla
    # config = Config.objects.get(pk=id_config)
    # Si es passa un registre no existent mostra la pàgina d'error 404 - Page not found
    config = get_object_or_404(Config, pk=id_config)
    return render(request, 'config/detail.html', {'config': config})
