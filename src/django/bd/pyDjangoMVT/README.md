 **_pyDjangoMVT_**
 
Projecte bàsic on es mostra informació dinàmica amb sites personalitzades ✨ 

**Índex**

[[_TOC_]]

## 1 - Agafa el projecte [pyDjangoModels](../pyDjangoModels/) com a referència 

En aquest projecte connectàvem una `app` django amb una base de dades remota postgresql i creàvem els models corresponents 
a les taules `tbl_config`, `tbl_empresa` i `tbl_usuari` 


## 2 - Recordem model E-R amb la següent imatge:

![new_project](../../resources/pycharm_django_models_ER.png "model ER..")

## 3 - Sobre el patró MVT
Les sigles MVT fan referència a **M**odel - **V**iew - **T**emplate utitlizant per django. En el moment que apareixen les
bases de dades apareix la necessitat de recuperar les dades i mostrar-les al navegador, si potser, d'una manera visualment atractiva i usable.

![mvt_pattern](../../resources/django_mvt.png "model MVT...")

## 4 Afegeix noves url --> urls.py

Com que estem fent proves utilitzarem un estil web bàsic on la informació, algunes vegades, no serà rellevant ni estarà relacionada però que mostrarà la forma d'obtenir-la.
Utilitza i adapta el codi següent:

````python
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', welcome)
]
````

## 5 - Crea les funcions corresponents --> views.py

Utilitza i adapta el codi següent:

`````python
def welcome(request):
    # objects es connectarà a la BBDD i ens retornarà informació
    # Consultar: https://docs.djangoproject.com/en/4.0/ref/models/class/
    n_configs = Config.objects.count()  # Nombre d'elements de la taula
    configs = Config.objects.all()  # Obté tots els elements ( select * from tbl_config )
    
    # Renderitza la vista a partir de la plantilla (template) --> welcome.html
    return render(request, 'welcome.html', context={'n_configs': n_configs, 'configs': configs})
`````

L'últim paràmetre de la funció `render` fa referència a un diccionari (`clau` : `valor`) anomenat `context`. Per conveni i en aquesta situació el nom de la `clau` coincidirà amb el del `valor`
D'aquesta manera podrem accedir a aquests valors sota el mateix nom tal com veurem a continuació.

## 6 - Crea una nova plantilla --> welcome.html
-
Al directori destinat a plantilles, anomenat `templates` al PyCharm crea un nou fitxer de tipus HTML versió 5.

`````html
<!DOCTYPE html>
<html lang="ca">
<head>
    <meta charset="UTF-8">
    <title>WELCOME</title>
</head>
<body>
<h1> Benvingut al sistema </h1>
<div>
    Nombre de configuracions al sistema: {{ n_configs }}
</div>
<div>
    <h2>
        Llistat de configuracions
    </h2>
    <div>
        <ul>
            {% for config in configs %}
                <li>ID: {{ config.id_config }},
                    MAX_INTENTS: {{ config.max_intents }},
                    PASS_MIN_LONG: {{ config.min_passwd_long }}
                </li>
            {% endfor %}
        </ul>
    </div>
</div>
</body>
</html>
`````

Com pots comprovar el codi tancat entre `{{ }}` no és típic del llenguatge HTML, de fet és python.

## 7 - Intentem fer-ho millor ;-)

El que anem a fer és que al polsar sobre un element, un hiper-enllaç, ens porti el detall del registre. La típica relació `master - detail`

Per aconseguir-ho primer haurem de crear una nova funció (views.py) que relacionarem amb una nova url (urls.py) o a l'inrevés i aquesta amb una nova template.
També és molt important saber el tipus de dades de la clau primària. Recorda que la clau primària d'una taula indica de manera unívoca un registre. 
L'ordre en les tasques és indiferent, tot depèn del teu parer.

**Definim la funció al fitxer views.py**
````python
def config_detail(request, id_config):
    """
    Obtenim el registre de la taula tbl_config identificat amb la seva clau primària id_config
    :param request: petició web
    :param id_config: identificador (clau primària) de la taula de la que obtenim el registre
    :return: pàgina web amb el detall del registre obtingut
    """
    # Si es passa un registre no existent falla
    # config = Config.objects.get(pk=id_config)
    # Si es passa un registre no existent mostra la pàgina d'error 404 - Page not found
    config = get_object_or_404(Config, pk=id_config)
    return render(request, 'config/detail.html', {'config': config})
````
**Definim el path o url d'accés a urls.py**
````python
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', welcome),
    path('config_detail/<int:id_config>', config_detail)
]
````
Fixa't que el path està indicat com `config_detail`/`<int:id_config>`. Com hauràs endevinat estem especificant:
* `config_detail`: una ruta. Potser qualsevol.
* `<int:id_config>`: tipus de dades `:` camp al que fem referència.

**Definim una nova template**

* Creem el directori `config` dins del directori `templates`
* Creem una nova plantilla que mostri el detall -> `config`/`detail.html`
```html
<!DOCTYPE html>
<html lang="ca">
<head>
    <meta charset="UTF-8">
    <title>Detall config</title>
</head>
<body>
    <h1>Detall de la configuració</h1>
    <div>Identificador           : {{ config.id_config }}</div>
    <div>Nombre màxim d'intents  : {{ config.max_intents }}</div>
    <div>Long mínima de passwords: {{ config.min_passwd_long }}</div>
</body>
</html>
```
* Adaptem la plantilla anterior -> `welcome.html`
````html
<!DOCTYPE html>
<html lang="ca">
<head>
    <meta charset="UTF-8">
    <title>WELCOME</title>
</head>
<body>
<h1> Benvingut al sistema </h1>
<div>
    Nombre de configuracions al sistema: {{ n_configs }}
</div>
<div>
    <h2>
        Llistat de configuracions
    </h2>
    <div>
        <ul>
            {% for config in configs %}
                <li>ID: {{ config.id_config }},
                    <a href="config_detail/{{ config.id_config }}">detall</a>
                </li>
            {% endfor %}
        </ul>
    </div>
</div>
</body>
</html>
````

Fixa't molt bé a la línia: `<a href="config_detail/{{ config.id_config }}">detall</a>`
* `href="config_detail/{{ config.id_config }}"`
  * `config_detail`: ruta d'accés. Aquesta ha de coincidir amb la definida anteriorment (`urls.py`)
  * `{{ config.id_config }}`: passem un nombre enter. Just el que esperàvem a (`urls.py`)


Fes proves amb altres taules.