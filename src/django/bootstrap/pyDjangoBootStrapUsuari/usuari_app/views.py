from django.shortcuts import render, redirect, get_object_or_404

from usuari_app.forms import UsuariForm
from usuari_app.models import Usuari


# Create
def usuari_create(request):
    """
    Crea un nou registre
    :param request: la petició de la web
    :return: la pàgina que insereix un nou registre
    """
    if request.method == 'POST':
        form = UsuariForm(request.POST or None)  # Obtenim tota la informació que ve del formulari
        if form.is_valid():  # Validem que la informació que ve del formulari és vàlida
            form.save()
            return redirect('home')
    else:  # Mostrem el formulari
        form = UsuariForm()
    return render(request, 'usuari_app/create.html', context={'form': form})


# Read (tots els registres)
def usuari_read(request):
    """
    Retorna tots els registres de la taula tbl_usuari
    :param request: la petició de la web
    :return: la pàgina principal (la llista d'usuaris)
    """
    usuaris = Usuari.objects.order_by('cognoms', 'nom')  # ORDER ASC primer per cognom, després per nom
    return render(request, 'usuari_app/home.html', context={'n_usuaris': Usuari.objects.count(), 'usuaris': usuaris})


# Read (solament 1 registre per mitjà de la clau principal
def usuari_detail(request, id_usuari):
    """
    Retorna 1 Usuari a partir del seu identificador (PK)
    :param request: la petició de la web
    :param id_usuari: Identificador de l'usuari (PK: int)
    :return: Pàgina de modificació
    """
    usuari = get_object_or_404(Usuari, pk=id_usuari)
    return render(request, 'usuari_app/detail.html', context={'usuari': usuari})


# Update (actualitza un usuari existent)
def usuari_update(request, id_usuari):
    """
    Modifica un usuari existent
    :param request: la petició de la web
    :param id_usuari: identificador de l'usuari (PK: int)
    :return: la pàgina que modifica l'usuari
    """
    usuari = get_object_or_404(Usuari, pk=id_usuari)
    if request.method == 'POST':
        usuari_form = UsuariForm(request.POST, instance=usuari)  # Obtenim tota la informació que ve del formulari
        if usuari_form.is_valid():  # Validem que la informació que ve del formulari és vàlida
            usuari_form.save()
            return redirect('home')
    else:  # Mostrem el formulari
        usuari_form = UsuariForm(instance=usuari)
    return render(request, 'usuari_app/create.html', context={'usuari_form': usuari_form})


# Delete (elimina un usuari)
def usuari_delete(request, id_usuari):
    """
    Elimina un usuari existent (sense confirmació)
    :param request: la petició de la web
    :param id_usuari: identificador de l'usuari (PK: int)
    :return: en eliminar la dada retorna a la pàgina principal
    """
    usuari = get_object_or_404(Usuari, pk=id_usuari)
    if usuari:  # Si existeix, és a dir, si ha retornat el registre
        usuari.delete()  # Eliminem el registre
    return redirect('home')
