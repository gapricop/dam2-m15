from django.contrib import admin
from django.urls import path

from usuari_app.views import usuari_read, usuari_create, usuari_detail, usuari_delete, usuari_update

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', usuari_read, name='home'),
    path('create/', usuari_create, name='create'),
    path('detail/<int:id_usuari>', usuari_detail, name='detail'),
    path('edit/<int:id_usuari>', usuari_update, name='update'),
    path('delete/<int:id_usuari>', usuari_delete, name='delete'),
]
