### updating....

# _Sobre django_
Django és un _framework_ web d'alt nivell que permet el desenvolupament ràpid de llocs web segurs i mantenibles. Desenvolupat per programadors i programadores experimentatades, django s'encarrega de gran part de les complicacions del desenvolupament web, per la qual cosa pots concentrar-te a escriure la teva aplicació sense necessitat de reinventar la roda. 
És ✨ gratuït i de codi obert ✨, té una comunitat pròspera i activa, una gran documentació i moltes opcions de suport gratuït i de pagament.

Consulta el lloc web: https://www.djangoproject.com/

## Instal·lació al PyCharm

1 - Crea un projecte python

2 - Ves al terminal del propi PyCharm. Comprova que estàs situat al directori del projecte

3- Pots fer-ho manualment o des de l'explorador de paquets integrat. Per fer-ho manualment, executa:
```sh
(venv) $ python -m pip install django
```

4- Una vegada instal·lat el _framework_, inicia un nou projecte des del terminal executant:
```sh
(venv) $ django-admin startproject nom_projecte
```
   On **nom_projecte** fa referència al nom del projecte que has utitlitzat.

   **TIP** _També pots seleccionar directament un projecte django des de l'assistent de nou projecte del PyCharm_

   El resultat de l'execució de l'ordre **django-admin** crearà un nou directori anomenat ***nom_projecte*** que contindrà la següent estructura
```sh
   ├── nom_projecte
   │   ├── __init__.py
   |   ├── asgi.py
   │   ├── settings.py
   │   ├── urls.py
   │   └── wsgi.py
   └── manage.py  
```

```sh __init__.py ``` Indica que els fitxers dintre del directori **nom_projecte** formen part d'un paquet (_package_) Python.

```sh asgi.py ``` permet, opcionalment, executar [Asynchronous Server Gateway Interface](https://channels.readthedocs.io/en/latest/asgi.html).

```sh settings.py ``` gestiona la configuració global del projecte django.

```sh urls.py ``` indica a django quines pàgines ha de construir en resposta al navegador o peticions URL.

```sh wsgi.py ``` significa **W**eb **S**erver **G**ateway **I**nterface que ajuda a django a servir les nostres pàgines web eventuals.


5- Per iniciar el projecte, situat al directori arrel del teu projecte. Trobaràs el fitxer **manage.py**. Executa
```sh
(venv) $ python manage.py runserver
```

6- Consulta el lloc web local: http://127.0.0.1:8000/


