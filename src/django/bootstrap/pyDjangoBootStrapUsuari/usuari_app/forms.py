# Creació de classes ModelForm personalizades
# Consultar https://docs.djangoproject.com/en/4.0/ref/models/fields/

from django.forms import (
    ModelForm,
    EmailInput,
    PasswordInput,
    NumberInput,
    CheckboxInput,
)

from usuari_app.models import Usuari


class UsuariForm(ModelForm):
    class Meta:
        model = Usuari
        fields = '__all__'
        # El widgets ens permeten personalitza el control que apareix a l'HTML
        # Consultar --> https://docs.djangoproject.com/en/4.0/ref/forms/widgets/
        widgets = {
            'email': EmailInput(attrs={'type': 'email'}),
            'password': PasswordInput(attrs={'type': 'password'}),
            'num_intents': NumberInput(attrs={'type': 'number'}),
            'bloquejat': CheckboxInput(attrs={'type': 'checkbox'}),
            'admin': CheckboxInput(attrs={'type': 'checkbox'})
        }
