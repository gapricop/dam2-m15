from django.db import models


# Consultar: https://docs.djangoproject.com/en/4.0/ref/models/fields/

class Config(models.Model):
    id_config = models.BigAutoField(primary_key=True)
    min_passwd_long = models.SmallIntegerField(default=8, null=True)
    max_intents = models.SmallIntegerField(default=5, null=True)

    # Es mostrarà a la GUI del django
    def __str__(self):
        return f'Config: Passwd: {self.min_passwd_long}\nMax_Intents: {self.max_intents}'

    class Meta:
        # managed = False  # Així no autocrea la taula
        db_table = 'tbl_config'  # Especifica el nom de la taula a la BBDD


class Empresa(models.Model):
    id_empresa = models.BigAutoField(primary_key=True)
    cif = models.CharField(unique=True,max_length=9)
    rao_social = models.CharField(max_length=120)

    # Es mostrarà a la GUI del django
    def __str__(self):
        return f'Empresa: [{self.id_empresa}] - {self.rao_social}'

    # Així especifiquem el nom de la taula
    class Meta:
        # managed = False  # Així no autocrea la taula
        db_table = 'tbl_empresa'  # Especifica el nom de la taula a la BBDD


class Usuari(models.Model):
    id_usuari = models.BigAutoField(primary_key=True)
    id_empresa = models.ForeignKey('Empresa', on_delete=models.CASCADE, db_column='id_empresa', null=False)
    email = models.CharField(unique=True, max_length=255)
    password = models.CharField(max_length=64, blank=True, null=True)
    bloquejat = models.BooleanField(blank=True, null=True, default=False)
    num_intents = models.SmallIntegerField(blank=True, null=True, default=0)
    nom = models.CharField(max_length=100, blank=True, null=True)
    cognoms = models.CharField(max_length=200, blank=True, null=True)
    admin = models.BooleanField(blank=True, null=True, default=False)

    # Es mostrarà a la GUI del django
    def __str__(self):
        return f'[{self.email}] - {self.cognoms}, {self.nom}: A:{self.admin}, B:{self.bloquejat}, N:{self.num_intents}'

    # Així especifiquem el nom de la taula
    class Meta:
        # managed = False  # Així no autocrea la taula
        db_table = 'tbl_usuari'  # Especifica el nom de la taula a la BBDD

