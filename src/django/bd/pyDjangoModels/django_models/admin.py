from django.contrib import admin

# Register your models here.
from django_models.models import Config, Empresa, Usuari

admin.site.register(Config)
admin.site.register(Empresa)
admin.site.register(Usuari)
