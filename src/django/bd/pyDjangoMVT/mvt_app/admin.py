from django.contrib import admin

# Register your models here.
from mvt_app.models import Config, Usuari, Empresa

admin.site.register(Config)
admin.site.register(Empresa)
admin.site.register(Usuari)
