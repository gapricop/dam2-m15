"""pyDjangoCRUDUsuari URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from webapp.views import welcome, usuari_detail, usuari_update, usuari_insert, usuari_delete

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', welcome, name='index'),  # Afegir name='nom' ens permet re-direccionar fàcilment. El nom identifica la ruta
    path('detail/<int:id_usuari>', usuari_detail),
    path('edit/<int:id_usuari>', usuari_update),
    path('insert/', usuari_insert),
    path('delete/<int:id_usuari>', usuari_delete),
]
