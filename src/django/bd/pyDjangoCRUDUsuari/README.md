 **_pyDjangoCRUDUsuari_**
 
Projecte bàsic on es realitzen les operacions *CRUD* (**C**reate **R**ead **U**pdate **D**elete) amb la taula `tbl_usuaris` que hem creat en projectes anteriors ✨ 

**Índex**

[[_TOC_]]

## 1 - Crear un projecte nou django 

Creem un nor projecte django amb l'assistent que ens proporciona el PyCharm. Recorda adaptar el nom del programa i de l'app al teu projecte.

![new_project](../../resources/pycharm_new_django_project.png "nou projecte...")

## 2 - Recordem model E-R amb la següent imatge:

![new_project](../../resources/pycharm_django_models_ER.png "model ER..")

❗Recorda instal·lar el driver de la base de dades [psycopg2-binary](https://pypi.org/project/psycopg2-binary/) i actualitzar el fitxer `settings.py`

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'X—>ASK<—X',
        'USER': 'X—>ASK<—X',
        'PASSWORD': 'X—>ASK<—X',
        'HOST': 'X—>ASK<—Xt',
        'PORT': 'X—>ASK<—X',
    }
}
```
## 3 - Creem una pàgina principal (mètode *Read*)

Començarem pel mètode més senzill `READ`. Aquest mètode ens permet obtenir la informació de la base de dades. Per a fer-ho crearem una pàgina principal que faci una consulta i ens mostri tots els registres. 
Recordem que cal actualitzar el fitxer `urls.py` i definir la seva vista a `views.py`, encara que abans haurem de crear els models de referència a `models.py`.

`models.py`: Creem el models de referència al fitxer `models.py`. Com que la taula `tbl_usuari` està referenciada amb `tbl_empresa` cal crear els 2 models perquè django interpreti correctament el model E-R definit anteriorment. 

❗️Recorda que l'atribut `managed`ens permet indicar a django si la taula es crea automàticament o ja està creada a la base de dades.
```python
class Empresa(models.Model):
    id_empresa = models.BigAutoField(primary_key=True)
    cif = models.CharField(unique=True, max_length=9)
    rao_social = models.CharField(max_length=120)

    # Es mostrarà a la GUI del django
    def __str__(self):
        return f'Empresa: [{self.id_empresa}] - {self.rao_social}'

    # Així especifiquem el nom de la taula
    class Meta:
        managed = False  # Així no autocrea la taula
        db_table = 'tbl_empresa'  # Especifica el nom de la taula a la BBDD


class Usuari(models.Model):
    id_usuari = models.BigAutoField(primary_key=True)
    id_empresa = models.ForeignKey('Empresa', on_delete=models.CASCADE, db_column='id_empresa', null=False)
    email = models.CharField(unique=True, max_length=255)
    password = models.CharField(max_length=64, blank=True, null=True)
    bloquejat = models.BooleanField(blank=True, null=True, default=False)
    num_intents = models.SmallIntegerField(blank=True, null=True, default=0)
    nom = models.CharField(max_length=100, blank=True, null=True)
    cognoms = models.CharField(max_length=200, blank=True, null=True)
    admin = models.BooleanField(blank=True, null=True, default=False)

    # Es mostrarà a la GUI del django
    def __str__(self):
        return f'[{self.email}] - {self.cognoms}, {self.nom}: A:{self.admin}, B:{self.bloquejat}, N:{self.num_intents}'

    # Així especifiquem el nom de la taula
    class Meta:
        managed = False  # Així no autocrea la taula
        db_table = 'tbl_usuari'  # Especifica el nom de la taula a la BBDD`
```

`urls.py`: Definim la url d'inici de l'aplicació. Fixa't que afegim `name='nom'`. El *nom* en permetrà re-direccionar a una pàgina utilitzant aquest valor. 

```python
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', welcome, name='index')  # Afegir name='nom' ens permet re-direccionar fàcilment. El nom identifica la ruta
]
````

`views.py`: Definim la vista de la pàgina inicial. Aquesta obté tots els registres de la taula *tbl_usuari* sense ordenar per cap atribut.
```python
def welcome(request):
    # Obté tots els registres sense ordenar
    usuaris = Usuari.objects.all()        
    return render(request, 'welcome.html', context={'n_usuaris': Usuari.objects.count(), 'usuaris': usuaris})
```

**Exemple de pàgina principal**

Deixem els enllaços preparats per les accions del CRUD. Recorda crear aquesta pàgina al teu directori de `templates`

```html
<!DOCTYPE html>
<html lang="ca">
<head>
    <meta charset="UTF-8">
    <title>Benvinguts al sistema</title>
</head>
<body>
<h1>Usuaris del sistema</h1>
<h2>Hi ha {{ n_usuaris }} usuaris al sistema</h2>
<h3><a href="insert">Agregar nou usuari...</a></h3>
<h1>El llistat d'usuaris és:</h1>
<div>
    <table border="1">
        <thead>
        <tr>
            <th>Id</th>
            <th>Nom</th>
            <th>EMail</th>
            <th></th> <!-- Per a l'enllaç Veure -->
            <th></th> <!-- Per a l'enllaç Editar -->
        </tr>
        </thead>
        <tbody>
        {% for usuari in usuaris %}
            <tr>
                <td>{{ usuari.id_usuari }}</td>
                <td>{{ usuari.cognoms }} {{ usuari.nom }}</td>
                <td>email: {{ usuari.email }}</td>
                <td><a href="detail/{{ usuari.id_usuari }}">Veure</a></td> <!-- Per mostrar el detall cal conèixer l'usuari seleccionat -->
                <td><a href="edit/{{ usuari.id_usuari }}">Modificar</a></td> <!-- Per modificar el detall cal conèixer l'usuari seleccionat -->
                <td><a href="delete/{{ usuari.id_usuari }}">Eliminar</a></td> <!-- Per eliminar el detall cal conèixer l'usuari seleccionat -->
            </tr>
        {% endfor %}
        </tbody>
    </table>
</div>
<br>
</body>
</html>
```

També podem afegir uan vista personalitzada per al detall d'un usuari concret amb el codi següent:

`templates`: Creem un directori que faci referència a allò que estem manipulant, per exemple *usuari*. Dintre crearem una nova pàgina que mostrarà el detall d'un usuari determinat.
````html
<!DOCTYPE html>
<html lang="ca">
<head>
    <meta charset="UTF-8">
    <title>Detall: {{ usuari.cognoms }}, {{ usuari.nom }}</title>
</head>
<body>
<div>
    {{ usuari.nom }},
    {{ usuari.cognoms }},
    {{ usuari.email }}
</div>
<!-- url 'index' Recorda que index fa referència al nom de la ruta. Consulta urls.py -->
<br><br><a href="{% url 'index' %}">Tornar a l'inici </a>
</body>
</html>
````
❗Fixa't que hem inserit al codi html el *re-direccionament* a la pàgina denominada `index`. 
Aquesta acció executarà el codi definit a la url. A l'exemple executarà el mètode *welcome* dek fitxer `views.py`


`views.py`:
```python
def usuari_detail(request, id_usuari):
    usuari = get_object_or_404(Usuari, pk=id_usuari)
    return render(request, 'usuari/detail.html', context={'usuari': usuari})
```

`urls.py`:
````python
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', welcome, name='index'),
    path('detail/<int:id_usuari>', usuari_detail)
]
````

## 4 - Mètode Create

El mètode `CREATE` permet inserir a la base de dades nova informació. Per a fer-ho caldrà definir la seva vista, la pàgina que ho gestionarà i la seva url.

Per a fer-ho, primer crearem una nova vista:

````python
# Create
def usuari_insert(request):
    if request.method == 'POST':
        usuari_form = UsuariForm(request.POST)  # Obtenim tota la informació que ve del formulari
        if usuari_form.is_valid():  # Validem que la informació que ve del formulari és vàlida
            usuari_form.save()
            return redirect('index')
    else:  # Mostrem el formulari
        usuari_form = UsuariForm()
    return render(request, 'usuari/insert.html', context={'usuari_form': usuari_form})
````

Seguidament actualitzarem el fitxer `urls.py` fent:

```python
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', welcome, name='index'),  # Afegir name='nom' ens permet re-direccionar fàcilment. El nom identifica la ruta
    path('detail/<int:id_usuari>', usuari_detail),    
    path('insert/', usuari_insert)    
]
```

Ara afegirem una nova pàgina *html* dintre del directori `templates/usuari`:

````html
<!DOCTYPE html>
<html lang="ca">
<head>
    <meta charset="UTF-8">
    <title>Nou registre</title>
</head>
<body>
<h1>Nou usuari</h1>
<form method="post">
    <table>{{ usuari_form }}</table>
    <button type="submit">Enviar...</button>
    {% csrf_token %}
</form>
<!-- url 'index' Recorda que index fa referència al nom de la ruta. Consulta urls.py -->
<br><br><a href="{% url 'index' %}">Tornar a l'inici </a>
</body>
</html>
````
❗És molt important afegir la línia `{% csrf_token %}`. Consulta la informació fent clic [aquí](https://docs.djangoproject.com/en/4.0/ref/csrf/=).

Podràs comprovar que el resultat del formulari d'inserció no és molt atractiu. Ni tan sols realitza una validació de les dades.
Per enriquir una mica el formulari i millorar-lo podem definir al fitxer de l'aplicació i de nova creació `forms.py` una sèrie de propietats que ens permetran definir el tipus d'entrada i el control.

Com observareu el que estem fent és personalitzar un [ModelForm](https://docs.djangoproject.com/en/4.0/ref/models/fields/).

``forms.py``:
````python
class UsuariForm(ModelForm):
    class Meta:
        model = Usuari  # Especifiquem a quin model fem referència
        fields = '__all__'  # Aquí podem dir quins camps retorna
        # El widgets ens permeten personalitza el control que apareix a l'HTML
        # Consultar --> https://docs.djangoproject.com/en/4.0/ref/forms/widgets/
        widgets = {
            'email': EmailInput(attrs={'type': 'email'}),
            'password': PasswordInput(attrs={'type': 'password'}),
            'num_intents': NumberInput(attrs={'type': 'number'}),
            'bloquejat': CheckboxInput(attrs={'type': 'checkbox'}),
            'admin': CheckboxInput(attrs={'type': 'checkbox'})
        }
````

`model = `: indicarem quin model del mòdul `models.py` estem personalitzant.

`fields = `: indicarem quins atributs del model retorna. Si podem `__all__` els retornarà tots.

`widgets =`: Especifiquem quin atribut de la classe del model anem a personalitzar. Per exemple:

    'email': EmailInput(attrs={'type': 'email'})

`email`: atribut email de la classe Usuari (a l'exemple)

`EmailInput()`: definim una regla de validació, en aquest cas verificarem que el que introdueix l'usuari és un email.

`attrs={'type': 'email'}`: especifiquem quin tipus de control anem a utilitzar.

Consulteu l'[API](https://docs.djangoproject.com/en/4.0/ref/forms/widgets/) per obtenir més informació sobre els widgets

## 5 - Mètode Update

Aquest mètode és molt semblant al *Create*. Solament requereixen uns pocs canvis:

``views.py``:
````python
def usuari_update(request, id_usuari):
    usuari = get_object_or_404(Usuari, pk=id_usuari)
    if request.method == 'POST':
        usuari_form = UsuariForm(request.POST, instance=usuari)  # Obtenim tota la informació que ve del formulari
        if usuari_form.is_valid():  # Validem que la informació que ve del formulari és vàlida
            usuari_form.save()
            return redirect('index')
    else:  # Mostrem el formulari
        usuari_form = UsuariForm(instance=usuari)
    return render(request, 'usuari/insert.html', context={'usuari_form': usuari_form})
````

❗Com observareu hem afegit el paràmetre `instance=` per fer referència a la instància de l'objecte que volem manipular.
Si no ho fem, no es modificarà res a la base de dades. D'aquesta manera indiquem quin usuari (a l'exemple) estem actualitzant.

`urls.py`: afegim un nou path per gestionar la modificació dels usuaris (en aquest exemple).
```python
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', welcome, name='index'),  # Afegir name='nom' ens permet re-direccionar fàcilment. El nom identifica la ruta
    path('detail/<int:id_usuari>', usuari_detail),    
    path('insert/', usuari_insert),
    path('edit/<int:id_usuari>', usuari_update)    
]
```

Crearem una nova plantilla a ``templates/usuari`` denominada update.html (per exemple).

❗Pots agafar la de l'operació d'inserció. És idèntica!!!

````html
<!DOCTYPE html>
<html lang="ca">
<head>
    <meta charset="UTF-8">
    <title>Actualitzant l'usuari amb id: {{ usuari.id_usuari }}</title>
</head>
<body>
<h1>Editar usuari ...</h1>
<form method="post">
    <table>{{ usuari_form }}</table>
    <button type="submit">Enviar...</button>
    {% csrf_token %}
</form>
<!-- url 'index' Recorda que index fa referència al nom de la ruta. Consulta urls.py -->
<br><br><a href="{% url 'index' %}">Tornar a l'inici </a>
</body>
</html>
````

## 6 - Mètode Delete

El procés d'eliminar un registre és encara més senzill ...

Com hem estat treballant ens caldrà definir una nova acció per la vista i un nou _path_.


En aquest cas no serà necessària una nova `template` ja que l'acció la tenim prevista a la pàgina principal i s'executarà al polsar sobre l'enllaç.

Recorda una part del codi de la plantilla (_template_) `welcome.html`:

`````html
<tbody>
    {% for usuari in usuaris %}
        <tr>
            <td>{{ usuari.id_usuari }}</td>
            <td>{{ usuari.cognoms }} {{ usuari.nom }}</td>
            <td>email: {{ usuari.email }}</td>
            <td><a href="detail/{{ usuari.id_usuari }}">Veure</a></td>
            <td><a href="edit/{{ usuari.id_usuari }}">Modificar</a></td>
            <td><a href="delete/{{ usuari.id_usuari }}">Eliminar</a></td>
        </tr>
    {% endfor %}
</tbody>
`````

Comencem per l'acció 👍

`views.py`:
````python
def usuari_delete(request, id_usuari):
    usuari = get_object_or_404(Usuari, pk=id_usuari)
    if usuari:  # Si existeix, és a dir, si ha retornat el registre
        usuari.delete()  # Eliminem el registre
    return redirect('index')
````

Com comprovaràs en fet de redirigir (_redirect_) executa el mètode `welcome()` i torna a carregar la informació.

Seguim amb el nou path, completant el mòdul:

`urls.py`:

```python
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', welcome, name='index'),  # Afegir name='nom' ens permet re-direccionar fàcilment. El nom identifica la ruta
    path('detail/<int:id_usuari>', usuari_detail),
    path('edit/<int:id_usuari>', usuari_update),
    path('insert/', usuari_insert),
    path('delete/<int:id_usuari>', usuari_delete),
]
```

Tot en conjunt et proporcionarà una forma bàsica de realitzar un CRUD típic.

Fes proves amb altres taules o amb altre projecte diferent 🤟