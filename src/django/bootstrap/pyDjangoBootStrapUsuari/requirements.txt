asgiref==3.5.0
beautifulsoup4==4.10.0
django-bootstrap5==21.3
Django==4.0.2
pip==22.0.2
psycopg2-binary==2.9.3
setuptools==60.7.0
soupsieve==2.3.1
sqlparse==0.4.2
wheel==0.37.1
