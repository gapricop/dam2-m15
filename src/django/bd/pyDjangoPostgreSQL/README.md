 # _pyDjangoPostgreSQL_
Projecte bàsic on es treballa la connexió a una base de dades remota postgresql ✨ 

## Passes al PyCharm

1 - Crea un projecte python de tipus django tal com hem fet en projectes anteriors. En l'apartat del nom de l'aplicació pots utilitzar de nom _hello_postgres_ per exemple. Personalitza-ho al teu gust.

2 - Si volem connectar el nostre projecte a una base de dades és necessari instal·lar el _driver_ al nostre entorn. Per a fer-ho instal·larem el paquet _psycopg2-binary_ del repositori paquets de Python [PyPi](https://pypi.org/). Aquest paquet ens permetrà connectar-se a una base de dades [PostgreSQL](https://www.postgresql.org/). Amb PyCharm pots fer-ho des del terminal o des de la finestra de _Python Packages_.

3 - Django necessita crear una estructura a la base de dades. Aquesta estructura es composa de diferents taules que, per defecte, porten el prefix _django__ i _auth__
```sh
   ├── base_de_dades
   │   ├── auth_group
   |   ├── auth_group_permissions
   │   ├── auth_permission
   │   ├── auth_user
   │   ├── auth_user_groups
   │   ├── auth_user_user_permissions
   │   ├── django_admin_log
   │   ├── django_content_type
   │   ├── django_migrations
   │   ├── django_session         
   │   ├── ... taules pròpies de l'app
   ·   ·
   ·   ·
```

Per crear aquesta estructura cal seguir una sèrie de passes:
 
3.1 - Crear una base de dades

Al servidor postgreSQL cal **[crear](https://www.arteco-consulting.com/gestion-de-usuarios-en-postgresql/)** una base de dades, un usuari i assignar-li els permisos adients.

3.2 - Configurar el projecte django perquè utilitzi la nova base de dades.

Recordem l'estructura d'un projecte django:
```sh
   ├── nom_projecte
   │   ├── __init__.py
   |   ├── asgi.py
   │   ├── settings.py
   │   ├── urls.py
   │   └── wsgi.py
   └── manage.py  
```

Manipularem el fitxer _settings.py_ per configurar l'accés al servidor [PostgreSQL](https://www.postgresql.org/). Django sempre crea una BBDD amb [sqlite3](https://www.sqlite.org/index.html) per defecte. Comenta el codi per si després ho necessites:
```python
DATABASES = {
     'default': {
         'ENGINE': 'django.db.backends.postgresql_psycopg2',
         'NAME': 'X-->ASK<--X',
         'USER': 'X-->ASK<--X',
         'PASSWORD': 'X-->ASK<--X',
         'HOST': 'X-->ASK<--X',
         'PORT': 'X-->ASK<--X',
     }
} 
```
`ENGINE`: Fa referència al driver de la BBDD que ututlizarà el programari.

3.4 - Realitzem la migració de dades. 

Les migracions relacionen les dades i les nostres classes de model. Django ho fa automàticament. Si volem conèixer les migracions pendents executarem el nostre programa. A la finestra _Run_ ens sorirà un missatge semblant a:

![new_project](../../resources/pycharm_django_migrations.png "django migrations...")

Amb més detall executarem:
```sh
python manage.py showmigrations
```
L'execució de l'ordre anterior ens mostrarà els canvis necessaris:

![new_project](../../resources/pycharm_django_showmigrations.png "showmigrations...")

Perquè django crei les migracions necessaries cal executar:
```sh
python manage.py migrate
```

La finestra _Run_ mostrarà alguna cosa semblant a:

![new_project](../../resources/pycharm_django_migrate.png "migrate...")

Si observem a la finestra _Database_ (_solament a la versió Professional_) o des d'un terminal comprovarem que s'han creat diferents elements:

![new_project](../../resources/pycharm_django_database.png "database...")

4 - Executem des del botó ▶ i anem al lloc web: http://127.0.0.1:8000/admin

Aquesta és la consola d'administració del nostre servidor django:

![new_project](../../resources/pycharm_django_admin_login.png "admin login...")

Per poder accedir és necessari crear un superusuari executant aquesta ordre:
```sh
python manage.py createsuperuser
```

A la consola ens sortirà un procés típic de creació d'usuaris ...

![new_project](../../resources/pycharm_django_create_superuser.png "create superuser...")

5 - Prova autenticar-te ;-)
