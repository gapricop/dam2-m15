from django.forms import modelform_factory
from django.shortcuts import render, get_object_or_404, redirect

# Create your views here.
from webapp.forms import UsuariForm
from webapp.models import Usuari

# Consultar: https://docs.djangoproject.com/en/4.0/ref/forms/models/
# Útil si no necessitem validar ni personalitzar els atributs al formulari web
# UsuariForm = modelform_factory(Usuari, exclude=[])


def welcome(request):
    # Obté tots els registres sense ordenar
    # usuaris = Usuari.objects.all()
    # Consultar --> https://docs.djangoproject.com/en/4.0/ref/models/querysets/#order-by
    # usuaris = Usuari.objects.order_by('-id_usuari')  # ORDER DESC
    # usuaris = Usuari.objects.order_by('id_usuari')  # ORDER ASC
    usuaris = Usuari.objects.order_by('cognoms', 'nom')  # ORDER ASC primer per cognom, desprès per nom
    return render(request, 'welcome.html', context={'n_usuaris': Usuari.objects.count(), 'usuaris': usuaris})


# Create
def usuari_insert(request):
    if request.method == 'POST':
        usuari_form = UsuariForm(request.POST)  # Obtenim tota la informació que ve del formulari
        if usuari_form.is_valid():  # Validem que la informació que ve del formulari és vàlida
            usuari_form.save()
            return redirect('index')
    else:  # Mostrem el formulari
        usuari_form = UsuariForm()
    return render(request, 'usuari/insert.html', context={'usuari_form': usuari_form})


# Read
def usuari_detail(request, id_usuari):
    usuari = get_object_or_404(Usuari, pk=id_usuari)
    return render(request, 'usuari/detail.html', context={'usuari': usuari})


# Update
def usuari_update(request, id_usuari):
    usuari = get_object_or_404(Usuari, pk=id_usuari)
    if request.method == 'POST':
        usuari_form = UsuariForm(request.POST, instance=usuari)  # Obtenim tota la informació que ve del formulari
        if usuari_form.is_valid():  # Validem que la informació que ve del formulari és vàlida
            usuari_form.save()
            return redirect('index')
    else:  # Mostrem el formulari
        usuari_form = UsuariForm(instance=usuari)
    return render(request, 'usuari/insert.html', context={'usuari_form': usuari_form})


# Delete
def usuari_delete(request, id_usuari):
    usuari = get_object_or_404(Usuari, pk=id_usuari)
    if usuari:  # Si existeix, és a dir, si ha retornat el registre
        usuari.delete()  # Eliminem el registre
    return redirect('index')
