from classes.Persona import Persona
from classes.Treballador import Treballador


def main():
    p = Persona('Rubén', 'Nadal')
    p.nom = 'Marc'
    print(p)
    t = Treballador(p.nom, p.cognoms, 5000)
    print(t)


if __name__ == '__main__':
    main()
