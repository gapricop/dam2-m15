 **_pyDjangoBootStrapUsuari_**
 
Projecte bàsic on es realitzen les operacions *CRUD* (**C**reate **R**ead **U**pdate **D**elete) amb la taula `tbl_usuaris` que hem creat en projectes anteriors millorant el seu aspecte amb el framework _bootstrap 5_ ✨ 

**Índex**

[[_TOC_]]

## 1. Instal·lar els requisits

```shell
pip3 install -r requirements.txt
```

_In progress..._