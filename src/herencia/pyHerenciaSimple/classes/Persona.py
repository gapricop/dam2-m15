class Persona:
    def __init__(self, nom, cognoms):
        self._nom = nom
        self._cognoms = cognoms

    @property
    def nom(self):
        return self._nom

    @nom.setter
    def nom(self, nom):
        self._nom = nom

    @property
    def cognoms(self):
        return self._cognoms

    @cognoms.setter
    def cognoms(self, cognoms):
        self._cognoms = cognoms

    def __str__(self) -> str:
        return f'Nom: {self._nom} Cognoms: {self._cognoms}'
