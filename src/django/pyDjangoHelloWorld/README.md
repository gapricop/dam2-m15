# _pyDjangoHelloWorld_
Projecte bàsic on es treballen diferents vistes (views.py) que mostren informació estàtica ✨ 

## Passes amb el PyCharm

1 - Crea un projecte python de tipus django

![new_project](../resources/pycharm_new_django_project.png "New project...")

2 - Recordem l'estructura d'un projecte django:
```sh
   ├── nom_projecte
   │   ├── __init__.py
   |   ├── asgi.py
   │   ├── settings.py
   │   ├── urls.py
   │   └── wsgi.py
   └── manage.py  
```
3 - Afegirem al fitxer _views.py_ de l'app creada 2 funcions diferents. Aquestes manipularan la vista corresponent:

```sh
   ├── HelloWorldApp
   │   ├── migrations
   |   ├── __init__.py
   |   ├── admin.py   
   │   ├── apps.py
   │   ├── models.py
   │   ├── tests.py
   └───┴── views.py      
```

Per exemple, pot quedar semblant a:

```sh
# Cal posar com paràmetre request --> conté les dades de la petició web
def welcome(request):
    return HttpResponse('Hola món !')


def bye(request):
    return HttpResponse('Adèu, fins la propera!')
```

4 - Modificarem el fitxer _urls.py_ del projecte django per afegir 3 noves rutes i les associarem a les noves vistes:

```sh
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', welcome),  # Si no té path és el primer que es mostra. Pàgina per defecte
    path('byebye.html', bye),  # Pot portar o no extensió
]
```

5 - Executem des del botó ▶️. Si tot va bé veurem a la finestra _Run_ missatges semblants a aquest:

![new_project](../resources/pycharm_django_server_run.png "django server...")

6 - Consulta el lloc web local: http://127.0.0.1:8000/ i comprova el resultat.

7 - Accedeix al lloc web local: http://127.0.0.1:8000/byebye.html . Surt el que esperaves ?

